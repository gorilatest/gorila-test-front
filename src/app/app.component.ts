import { Component } from '@angular/core';

import { routerTransition, fadeAnimation } from './interface/';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  animations: [ routerTransition ],
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

}
