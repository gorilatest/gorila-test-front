import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';

import { StartModule } from './start/start.module';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./start/start.module').then(m => m.StartModule),
    data: { state: 'start' }
  },
  {
    path: 'inicio',
    loadChildren: () => import('./start/start.module').then(m => m.StartModule),
    data: { state: 'inicio' }
  },
  {
    path: 'calcular-cdb',
    loadChildren: () => import('./cdb-test/cdb-test.module').then(m => m.CdbTestModule),
    data: { state: 'cdb' }
  },
  {
    path: '**',
    component: NotFoundComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}
