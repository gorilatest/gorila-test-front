import { Component, OnInit } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { DatePipe } from '@angular/common';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

import { CDBModel } from './cdb.model';

import { CdbTestService } from './cdb-test.service';

import moment, { Moment } from 'moment';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4lang_pt_BR from "@amcharts/amcharts4/lang/pt_BR";

@Component({
  selector: 'app-cdb-test',
  templateUrl: './cdb-test.component.html',
  styleUrls: ['./cdb-test.component.sass'],
  providers: [
  ],
})
export class CdbTestComponent implements OnInit{

  // contexto dos campos de data
  contextDate = {
    minDate: new Date('01/04/2010'),
    maxDate: new Date('12/26/2016'),
    minEndDate: new Date('11/14/2016'),
    maxEndDate: new Date('12/03/2019')
  };
  
  msgError: String = null;
  isLoading: Boolean = false;
  showResult: Boolean = false;

  contextResult = {
    investmentDate: new Date(),
    currentDate:  new Date(),
    finalUnitPrice: 0
  }

  cdbCalcForm: FormGroup;

  // instancia do grafico
  graphicCdb:GraphicCdb;

  constructor(private formBuilder: FormBuilder, private cdbTestService:CdbTestService) {
    this.initContextForm();
  }

  // inicia o contexto (campos) do formulário
  initContextForm(){
    this.cdbCalcForm = this.formBuilder.group({
      investmentDate: [new Date('11/14/2016').toISOString(), [Validators.required] ],
      currentDate: [new Date('12/26/2016').toISOString(), [Validators.required] ],
      cdbRate: ['103.5', [Validators.required] ],
    });
  }

  // - - - - - - - - - - - - - - - - - - - - - - - - -

  get f() { return this.cdbCalcForm.controls; }

  // - - - - - - - - - - - - - - - - - - - - - - - - -

  // mascara usada no campo data
  customMask(e){
    if( ! /[0-9]|\/|[:cntrl:]/.test(e.key) ){
      return false;
    }
  }

  // esconde o resultado enquanto manipular o formulário
  hideResult(){
    this.showResult = false;
  }

  // trata a alteração de data no input pelo usuário
  changeDateInput(e, field){
    const dateString = e.target.value.split('/');
    this.f[field].setValue( new Date( dateString[2], dateString[1]-1, dateString[0] ).toISOString() );
    this.hideResult();
  }

  // set data máxima da data final ao mudar a data inicial
  changeDate(event: MatDatepickerInputEvent<Date>) {
    const selectedDate = event.targetElement['value'];
    this.contextDate.minEndDate = new Date( selectedDate );
    this.hideResult();
  }
  
  // set data máxima da data inicial ao mudar a data final
  changeDateEnd(event: MatDatepickerInputEvent<Date>) {
    const selectedDate = event.targetElement['value'];
    this.contextDate.maxDate = new Date( selectedDate );
    this.hideResult();
  }

  // método submit do formulário para calcular o resultado
  calcCDB(){
    
    this.msgError = null;
    this.hideResult();
    if (this.cdbCalcForm.invalid) {
      return;
    }

    this.isLoading = true;
    this.showResult = false;

    // dados para o serviço usando a Interface CDBModel
    const cdbData: CDBModel = {
      investmentDate: this.f.investmentDate.value,
      cdbRate: this.f.cdbRate.value,
      currentDate: this.f.currentDate.value
    }

    this.cdbTestService.getCalc(cdbData).subscribe(  
      content =>{
        this.graphicCdb.render(content.resultCalculed);
        this.isLoading = false;
        this.showResult = true;
        const investmentDate = content.resultCalculed[0].date.split('-');
        const currentDate = content.resultCalculed[content.resultCalculed.length-1].date.split('-');
        this.contextResult.investmentDate = this.f.investmentDate.value; //new Date( Date.UTC(investmentDate[0], parseInt(investmentDate[1])-1, investmentDate[2]) );
        this.contextResult.currentDate = this.f.currentDate.value; //new Date( Date.UTC(currentDate[0], parseInt(currentDate[1])-1, currentDate[2]) );
        this.contextResult.finalUnitPrice = content.resultCalculed[content.resultCalculed.length-1].unitPrice;
      },
      error => {
        this.isLoading = false;
        this.showResult = false;
        this.msgError = 'Ocorreu um erro no serviço.';
        console.error('Ocorreu um erro no serviço: ', error)
      }
    );
  }

  ngOnInit(): void {
    // instanciando o objeto de gráfico
    this.graphicCdb = new GraphicCdb();
  }
}

// classe para renderizar o gráfico como resultado
class GraphicCdb {

  chart = am4core.create("resultCalcCDB", am4charts.XYChart);

  constructor(){
    this.init();
  }

  init(){
    
    let chart = this.chart;
    chart.paddingRight = 20;
    chart.paddingLeft = 0;
    chart.language.locale = am4lang_pt_BR;
    // Create axes
    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.minGridDistance = 50;

    chart.fontSize = 11;
    chart.fontFamily = ('Lato');

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

    // Create series
    var series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = "value";
    series.dataFields.dateX = "date";
    series.strokeWidth = 2;
    series.minBulletDistance = 10;
    series.tooltipText = "{valueY}";
    series.tooltip.pointerOrientation = "vertical";
    series.tooltip.background.cornerRadius = 20;
    series.tooltip.background.fillOpacity = 0.5;
    series.tooltip.label.padding(12,12,12,12);
    series.tooltip.label.fontSize = 11;
    series.tooltip.label.fontFamily = ('Lato');
    series.tooltip.fontSize = 11;
    series.tooltip.fontFamily = ('Lato');

    chart.numberFormatter.numberFormat = "R$ #.###,##";
    // Add scrollbar
    chart.scrollbarX = new am4charts.XYChartScrollbar();
    chart.scrollbarX['series'].push(series);

    // Add cursor
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.xAxis = dateAxis;
    chart.cursor.snapToSeries = series;
  }

  render(listCdb){
    const _this = this;
    let chart = this.chart;

    let data = [];
    listCdb.map( cdbDate => {
      const date = cdbDate.date.split('-');
      data.push({ date: new Date(date[0], date[1]-1, date[2]), value: (cdbDate.unitPrice) });
    });

    chart.data = data;

    chart.validateData();
  }
}