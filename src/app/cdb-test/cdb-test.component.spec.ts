import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdbTestComponent } from './cdb-test.component';

describe('CdbTestComponent', () => {
  let component: CdbTestComponent;
  let fixture: ComponentFixture<CdbTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdbTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdbTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
