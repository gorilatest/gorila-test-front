import mongoose from 'mongoose';
export class CDBModel {
  investmentDate: Date;
  cdbRate: mongoose.Types.Decimal128;
  currentDate: Date;
}