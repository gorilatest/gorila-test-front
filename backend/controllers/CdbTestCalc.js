
class CdbTestCalc{

  // método para calcular e gerar lista de resultados diários calculados
  calc(listCdbDates, cdbRate){
    const listCalcDates = [];
    let  tcdiAccrued  = 1;

    listCdbDates.map( cdbDate => {
      const tcdi =  this.getTCDI(cdbDate.price);
      tcdiAccrued = tcdiAccrued * (this.getTCDIAccrued(tcdi, cdbRate));
      listCalcDates.push(
        {
          date: `${new Date(cdbDate.date).getFullYear()}-${(new Date(cdbDate.date).getMonth()+1)}-${new Date(cdbDate.date).getDate()}`,
          unitPrice: parseFloat((1000 * tcdiAccrued).toFixed(2))
        }
      );
    });
    return listCalcDates;
  }

  //calculo de TCDI usando fórmula do teste
  getTCDI(cdbPrice){
    return parseFloat( ( Math.pow(((cdbPrice/100) + 1 ), 1/252) - 1 ).toPrecision(5) );
  };

  //calculo de TCDI acumulado usando fórmula do teste
  getTCDIAccrued(tcdi, cdbRate){
    const tcdiA =  (1 + ( tcdi * (cdbRate/100) ));
    /* teste de formatação usando es6
    const formatter = new Intl.NumberFormat('en-US', {
        minimumFractionDigits: 16,      
        maximumFractionDigits: 16,
    });*/
    return parseFloat( tcdiA.toFixed(16) );
  };

}

export default CdbTestCalc;